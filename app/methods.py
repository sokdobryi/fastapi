import os
from datetime import datetime
import math
import PyPDF2
from docx import Document
import app.db_models
from app.db_connect import SessionLocal


UPLOADED_FILES_PATH = "uploads/"


def get_file_from_db(name: str = None):
    db = SessionLocal()
    if name is None:
        q = db.query(app.db_models.Image).all()
    else:
        q = db.query(app.db_models.Image).filter(app.db_models.Image.name == name).first()
    db.close()
    return q


def get_files_from_db_limit_offset(query, limit: int = None, offset: int = None):
    if limit and not offset:
        query = query[:limit]
    elif limit and offset:
        limit += offset
        query = query[offset:limit]
    elif not limit and offset:
        query = query[offset:]
    return query


def delete_file_from_uploads(file, tag):
    if tag:
        try:
            os.remove(f'{UPLOADED_FILES_PATH}{tag}/{file}')
            files = os.listdir(f'{UPLOADED_FILES_PATH}{tag}/')
            if len(files) == 0:
                os.rmdir(f'{UPLOADED_FILES_PATH}{tag}')
        except Exception as e:
            print(e)
    else:
        try:
            os.remove(f'{UPLOADED_FILES_PATH}{file}')
        except Exception as e:
            print(e)


async def save_file_to_uploads(file, tag):
    if tag:
        if not os.path.isdir(f'{UPLOADED_FILES_PATH}{tag}'):
            os.mkdir(f'{UPLOADED_FILES_PATH}{tag}')
        with open(f'{UPLOADED_FILES_PATH}{tag}/{file.filename}', "wb") as uploaded_file:
            file_content = await file.read()
            uploaded_file.write(file_content)
            uploaded_file.close()
    else:
        with open(f'{UPLOADED_FILES_PATH}{file.filename}', "wb") as uploaded_file:
            file_content = await file.read()
            uploaded_file.write(file_content)
            uploaded_file.close()


def format_filename(file_id=None, name=None):
    if name is None:
        filename = str(file_id)
    else:
        filename = name
    return filename


def get_file_size(filename, tag):
    if tag:
        file_path = f'{UPLOADED_FILES_PATH}{tag}/{filename}'
        return os.path.getsize(file_path)
    else:
        file_path = f'{UPLOADED_FILES_PATH}{filename}'
        return os.path.getsize(file_path)


def add_file_to_db(**kwargs):
    db = SessionLocal()
    new_file = app.db_models.Image(
        annotation=kwargs['annotation'],
        name=kwargs['full_name'],
        tag=kwargs['tag'],
        size=kwargs['file_size'],
        mime_type=kwargs['file'].content_type,
        modification_time=datetime.now(),
    )
    db.add(new_file)
    db.commit()
    db.refresh(new_file)
    return new_file


def update_file_in_db(**kwargs):
    db = SessionLocal()
    update_file = db.query(app.db_models.Image).filter(app.db_models.Image.file_id == kwargs['file_id']).first()
    update_file.name = kwargs['full_name']
    update_file.tag = kwargs['tag']
    update_file.annotation = kwargs['annotation']
    update_file.mime_type = kwargs['file'].content_type
    update_file.modification_time = datetime.now()
    db.commit()
    db.refresh(update_file)
    return update_file


def delete_file_from_db(file_info_from_db):
    db = SessionLocal()
    db.delete(file_info_from_db)
    db.commit()


def get_content_file(name: str, tag: str):
    get_file_from_db(name)
    with open(f"{UPLOADED_FILES_PATH}/{tag}/{name}", "rb") as f:
        bytes_f = f.read()
        return bytes_f


def search_text_in_pdf(file, substr):
    file_path = f'{UPLOADED_FILES_PATH}/{file.tag}/{file.name}'
    pdf = open(file_path, 'rb')
    reader = PyPDF2.PdfReader(pdf)
    num_pages = len(reader.pages)
    for page_number in range(num_pages):
        page = reader.pages[page_number]
        text = page.extract_text()

        if substr in text:
            return file.name
    pdf.close()
    return


def search_text_in_docx(file, substr):
    file_path = f'{UPLOADED_FILES_PATH}{file.name}'
    doc = Document(file_path)
    for para in doc.paragraphs:
        if substr.lower() in para.text.lower():
            return file.name
    return


def human_read_format(size):
    pwr = math.floor(math.log(size, 1024))
    stuff = ["Б", "КБ", "МБ", "ГБ", "ТБ", "ПБ", "ЭБ", "ЗБ", "ЙБ"]
    if size > 1024 ** (len(stuff) - 1):
        return "Incorrect size"
    return f"{size / 1024 ** pwr:.0f} {stuff[pwr]}"
