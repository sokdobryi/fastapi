import os
import app
from app.auth import get_user
from app.db_connect import SessionLocal
from app.db_models import User, Prepods


UPLOADED_FILES_PATH = "static/img/prepods"


def get_prepods(logger) -> list:
    """
    Function for getting all teachers from database
    :param logger: logger
    :return: list of Prepods
    """
    try:
        db = SessionLocal()
        prepods = db.query(app.db_models.Prepods).all()
        db.close()
        return prepods
    except Exception as error:
        logger.error(f'Error in line 21 prepods.py // {error}')
        return list()


def get_prepod(title: str, logger):
    """
    Function for getting teacher with this title from database
    :param title: title of teacher
    :param logger: logger
    :return: info about teacher
    """
    try:
        db = SessionLocal()
        prepod = db.query(app.db_models.Prepods).filter(app.db_models.Prepods.title == title).first()
        db.close()
        return prepod
    except Exception as error:
        logger.error(f'Error in line 39 prepods.py // {error}')


def create_prepod(title: str, description: str, foto: str, owner_id: int, logger):
    """
    Function for create prepod with this params
    :param title: title of new teacher
    :param description: description of new teacher
    :param foto: photo of new teacher
    :param logger: logger
    :param owner_id: id owner
    :return: model of new teacher
    """
    try:
        db = SessionLocal()
        owner = get_user(id=owner_id)

        prepod = Prepods(title=title,
                         description=description,
                         avatar=foto,
                         owner_id=owner_id,
                         owner=owner)
        db.add(prepod)
        db.commit()
        db.refresh(prepod)
        db.close()
        logger.info(f'{prepod} created successful')
        return prepod
    except Exception as error:
        logger.error(f'Error in line 61 prepods.py // {error}')


def delete_prepod(prepod, logger):
    """
    Function for delete teacher
    :param prepod: model of teacher
    :param logger: logger
    :return: None
    """
    try:
        db = SessionLocal()
        os.remove(f'{UPLOADED_FILES_PATH}/{prepod.avatar}')
        db.delete(prepod)
        db.commit()
        db.close()
        logger.info(f'teacher {prepod.title} successful delete')
    except Exception as error:
        logger.error(f'Error in line 79 prepods.py // {error}')


def update_prepod(id: int, title: str, description: str, avatar: str, owner_id: int, logger) -> None:
    """
    Function for update info of teacher
    :param id: id of teacher
    :param title: new title
    :param description: new description
    :param avatar: new avatar
    :param logger: logger
    :return: None
    """
    try:
        db = SessionLocal()
        owner = get_user(id=owner_id)

        update_file = db.query(app.db_models.Prepods).filter(app.db_models.Prepods.id == id).first()
        update_file.owner = owner
        update_file.owner_id = owner_id
        update_file.title = title
        update_file.description = description
        update_file.url_photo = avatar
        db.commit()
        db.refresh(update_file)
    except Exception as error:
        logger.error(f'Error in line 100 prepods.py // {error}')


def get_prepod_by_id(id: int, logger):
    """
    Function for getting teacher by id
    :param id: identification of teacher
    :param logger: logger
    :return: model of teacher
    """
    try:
        db = SessionLocal()
        prepod = db.query(app.db_models.Prepods).filter(app.db_models.Prepods.id == id).first()
        db.close()
        return prepod
    except Exception as error:
        logger.error(f'Error in line 115 prepods.py // {error}')
