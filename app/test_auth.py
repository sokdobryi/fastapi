from fastapi.testclient import TestClient

from app.main import server

client = TestClient(server)


def get_token(username, password):
    param = {
        "grant_type": "",
        "username": username,
        "password": password,
        "scope": "",
        "client_id": "",
        "client_secret": ""
    }
    response = client.post("http://0.0.0.0:8000/token", data=param)
    token = response.json()["access_token"]
    return token


def register_user(username, password):
    response = client.post(f"http://0.0.0.0:8000/register?username={username}&password={password}")
    return response


def test_read_main():
    response = client.get("/")
    assert response.status_code == 404
    # assert response.json() == {"msg": "Hello World"}


def test_register_user():
    response = register_user("b", "b")
    assert response.status_code == 200
    assert response.json() == {
        "username": "b",
        "role": 1,
    }


def test_get_token():
    token = get_token("b", "b")
    response = client.get("http://0.0.0.0:8000/protected", headers={"Authorization": f"Bearer {token}"})
    # assert response.status_code == 200
    assert response.json() == ["Hello, user"]


def test_user_info_without_git():
    token = get_token("b", "b")
    response = client.get("http://0.0.0.0:8000/user", headers={"Authorization": f"Bearer {token}"})
    assert response.json()["id"] == 1
    assert response.json()["git_id"] is None
    assert response.json()["git_name"] is None
    assert response.json()["git_avatar_url"] is None
    assert response.json()["git_web_url"] is None
    assert response.json()["role"] == 1
    assert response.json()["username"] == "b"
    assert response.json()["git_username"] is None


def test_user_info_with_git():
    token = get_token("b", "b")
    response = client.get("http://0.0.0.0:8000/test/git", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == 200

    param = {"id": "1771056", "username": "14NAGIBATOR88", "name": "Ilya", "state": "active",
             "avatar_url": "https://secure.gravatar.com/avatar/51f11cd7498e7df3c89e9a900ebd9978?s=80&d=identicon",
             "web_url": "https://gitlab.com/14NAGIBATOR88", "created_at": "2017-11-11T13:59:00.893Z", "bio": "",
             "location": "", "public_email": "", "skype": "", "linkedin": "", "twitter": "", "discord": "",
             "website_url": "", "organization": "", "job_title": "", "pronouns": "null", "bot": "false",
             "work_information": "null", "local_time": "7:52 PM", "last_sign_in_at": "2023-08-20T20:59:03.072Z",
             "confirmed_at": "2017-11-11T13:59:34.932Z", "last_activity_on": "2023-09-10",
             "email": "ilya.shlyahetko@gmail.com", "theme_id": 1, "color_scheme_id": 2, "projects_limit": 100000,
             "current_sign_in_at": "2023-09-10T19:17:26.115Z", "identities": [], "can_create_group": "true",
             "can_create_project": "true", "two_factor_enabled": "false", "external": "false",
             "private_profile": "false", "commit_email": "ilya.shlyahetko@gmail.com",
             "shared_runners_minutes_limit": "null", "extra_shared_runners_minutes_limit": "null",
             "scim_identities": []}
    response = client.post("http://0.0.0.0:8000/test/auth/git", params=param)
    print(response)

    assert response.status_code == 200
    token = response.json()["access_token"]
    response = client.get("http://0.0.0.0:8000/user", headers={"Authorization": f"Bearer {token}"})
    assert response.json()["id"] == 1
    assert response.json()["git_id"] == 1771056
    assert response.json()["git_name"] == "Ilya"
    assert response.json()["git_avatar_url"] == "https://secure.gravatar.com/avatar/51f11cd7498e7df3c89e9a900ebd9978?s=80&d=identicon"
    assert response.json()["git_web_url"] == "https://gitlab.com/14NAGIBATOR88"
    assert response.json()["role"] == 1
    assert response.json()["username"] == "b"
    assert response.json()["git_username"] == "14NAGIBATOR88"

def test_admin_get_users():
    client.get("http://0.0.0.0:8000/test/register")
    admin_token = get_token("a", "a")
    response = client.get("http://0.0.0.0:8000/admin/get-users", headers={"Authorization": f"Bearer {admin_token}"})
    assert len(response.json()) == 2

def test_admin_change_user():
    user_token = get_token("b", "b")
    response = client.get("http://0.0.0.0:8000/user", headers={"Authorization": f"Bearer {user_token}"})
    first_id = response.json()["id"]

    admin_token = get_token("a", "a")
    param = {
        "id": 2,
        "password": "c"
    }
    response = client.post("/admin/reset-password?id=2&password=c", headers={"Authorization": f"Bearer {admin_token}"})
    # print(response)
    # user_token = get_token("b", "c")
    # response = client.get("http://0.0.0.0:8000/user", headers={"Authorization": f"Bearer {user_token}"})
    # second_id = response.json()["id"]
    # assert first_id == second_id


