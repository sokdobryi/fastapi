import re


def filename_check(input_text):
    pattern = r"^(.*?)\. (.*?) \((\d{4})\)$"
    result = re.match(pattern, input_text)
    output = []
    if result:
        output.append(result.group(1))
        output.append(result.group(2))
        output.append(result.group(3))
        return output
