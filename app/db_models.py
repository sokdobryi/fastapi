import enum

from sqlalchemy.orm import relationship
from datetime import datetime
from sqlalchemy import Column, Integer, String, Enum, ForeignKey, DateTime, null, Boolean
from app.db_connect import Base


class Roles(enum.Enum):
    user = 1
    employee = 2
    admin = 3


class Image(Base):
    __tablename__ = 'images'

    id = Column(Integer, primary_key=True, index=True)
    annotation = Column(String, nullable=True, default=null())
    name = Column(String)
    tag = Column(String)
    size = Column(String)
    mime_type = Column(String)
    modification_time = Column(String)


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    role = Column(Enum(Roles))
    hashed_password = Column(String, nullable=True, default=null())
    avatar = Column(String, nullable=True)
    git_id = Column(Integer, nullable=True, default=null())


class News(Base):
    __tablename__ = "news"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, unique=False, index=True)
    description = Column(String, unique=True, index=True)
    created_date = Column(String, default=datetime.now().strftime('%Y-%m-%d'))
    url_photo = Column(String, default="/app/uploads/news_photo/")
    genre = Column(String, default="other")
    completed_date = Column(DateTime, nullable=True, default=None)
    complete = Column(Boolean, default=False)
    comments = relationship("Comment", back_populates="news", cascade="all, delete-orphan")


class Comment(Base):
    __tablename__ = "comments"

    id = Column(Integer, primary_key=True, index=True)
    text = Column(String)
    news_id = Column(Integer, ForeignKey("news.id"))
    owner_id = Column(Integer)
    
    news = relationship("News", back_populates="comments")


class Prepods(Base):
    __tablename__ = "prepods"

    id = Column(Integer, primary_key=True, index=True)
    avatar = Column(String, unique=True, nullable=True)
    title = Column(String, unique=True, nullable=True, index=True)
    description = Column(String, unique=True, nullable=True, index=True)

    owner_id = Column(Integer, ForeignKey("users.id"), nullable=True, default=None)
    owner = relationship('User', backref='owner_prepods', foreign_keys=[owner_id], lazy="joined")



