import os
import shutil
from datetime import datetime

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from passlib.context import CryptContext
from pydantic import BaseModel
from pydantic.datetime_parse import timedelta
from sqlalchemy import null

import app
from app.db_connect import SessionLocal
from app.db_models import User

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


def get_password_hash(password):
    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_user(username: str = None, id: int = None):
    db = SessionLocal()
    if id is None:
        user = db.query(User).filter(User.username == username).first()
    else:
        user = db.query(User).filter(User.id == id).first()
    db.close()
    return user


# def update_user()

def authenticate_user(username: str, password: str):
    user = get_user(username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user(username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def create_user(username: str, password: str, role: str):
    db = SessionLocal()
    hashed_password = get_password_hash(password)
    create_user = User(username=username,
                       hashed_password=hashed_password,
                       role=role,
                       git_id=null(),
                       )
    db.add(create_user)
    db.commit()
    db.refresh(create_user)
    db.close()
    return create_user


def git_update_user(id: str, data: dict):
    db = SessionLocal()
    update_user = db.query(app.db_models.User).filter(app.db_models.User.id == id).first()
    update_user.git_id = data["git_id"]
    update_user.git_username = data["git_username"]
    update_user.git_name = data["git_name"]
    db.commit()
    db.refresh(update_user)
    return update_user


def authenticate_git_user(data: dict):
    db = SessionLocal()
    git_id = data["id"]
    user = db.query(app.db_models.User).filter(app.db_models.User.git_id == git_id).first()
    # user = db.query(User).filter(User.git_id == git_id).first()
    if not user:
        return False
        # return "Not registered or not union with git"
    db.close()
    return user


def update_user(id: str, username: str, role: str):
    db = SessionLocal()
    update_user = db.query(app.db_models.User).filter(app.db_models.User.id == id).first()
    update_user.username = username
    update_user.role = role
    db.commit()
    db.refresh(update_user)
    return update_user


def update_user_role(id: int, new_role: str):
    db = SessionLocal()
    update_user = db.query(app.db_models.User).filter(app.db_models.User.id == id).first()
    update_user.role = new_role
    db.commit()
    db.refresh(update_user)
    return update_user


def reset_password(id: str, password: str):
    db = SessionLocal()
    update_user = db.query(app.db_models.User).filter(app.db_models.User.id == id).first()
    hashed_password = get_password_hash(password)
    update_user.hashed_password = hashed_password
    db.commit()
    db.refresh(update_user)
    return update_user


def get_users():
    db = SessionLocal()
    users = db.query(app.db_models.User).all()
    db.close()
    return users


def delete_user(user):
    shutil.rmtree(user.username)
    db = SessionLocal()
    db.delete(user)
    db.commit()
    db.close()


def change_avatar_url(user, new_avatar_url):
    db = SessionLocal()
    update_user = db.query(app.db_models.User).filter(app.db_models.User.id == user.id).first()
    update_user.avatar = "/app/static/img/users/" + new_avatar_url
    db.commit()
    db.close()

    return create_user


def get_user_by_id(user_id: int):
    db = SessionLocal()
    user = db.query(app.db_models.User).filter(app.db_models.User.id == user_id).first()
    return user


def auth_user_git(data: dict):
    db = SessionLocal()
    git_id = data["id"]
    user = db.query(app.db_models.User).filter(app.db_models.User.git_id == git_id).first()
    if user:
        return user
    git_avatar_url = data["avatar_url"]
    git_username = data["username"]
    create_user = User(git_id=git_id,
                       avatar=git_avatar_url,
                       username=git_username,
                       role="employee"
                       )
    db.add(create_user)
    db.commit()
    db.refresh(create_user)
    db.close()
    return create_user
