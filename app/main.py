from typing import Optional

import httpx
import logging
import random
import app.db_models

from app.auth import *
from app.news import *
from pathlib import Path
from app.methods import *
from app.prepods import *
from datetime import timedelta
from starlette.staticfiles import StaticFiles
from starlette.status import HTTP_302_FOUND
from fastapi.responses import RedirectResponse
from fastapi import FastAPI, Response, status, Depends, File, UploadFile, HTTPException, Request, Header, Body, Form
from starlette.responses import FileResponse
from app.db_connect import engine, SessionLocal
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.templating import Jinja2Templates

UPLOADED_IMG_PATH_PREPODS = "static/img/prepods"
UPLOADED_IMG_PATH_USERS = Path() / 'static/img/users'
UPLOADED_FILES_PATH = Path() / 'uploads'
UPLOADED_IMG_PATH = "static/img/news"

authorizationUrl = "https://gitlab.com/oauth/authorize"
tokenUrl = "https://gitlab.com/oauth/token"
scope = "read_user"

client_id = "c4d8e78028451515aad2d7180a06f20f66d006399bb1db6406f9173084236ffa"
client_secret = "6b623d758fedae3ffe913520e0c0ced8063123ec191c1c62723009d70c1303b7"
redirect_uri = f'http://localhost:8000/auth/callback'

server = FastAPI()
os.chdir("app")

app.db_models.Base.metadata.create_all(engine)
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename="../main.log")
logger.addHandler(handler)
handler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))

templates = Jinja2Templates(directory="templates")
server.mount("/app/static", StaticFiles(directory="static"), name="static")


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@server.get('/', tags=["Main"])
def home(request: Request):
    #create_user("a", "a", "admin")
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" url=\"/\" status=\"HTTP_200_OK\"')
    news = get_posts(logger=logger)[::-1]
    count_of_news = len(news)
    if_news = len(news)
    return templates.TemplateResponse("index.html", {"request": request,
                                                     "news": news,
                                                     "if_news": if_news,
                                                     "count": count_of_news
                                                     })


"""
_______________________ News block ___________________________
"""


@server.post("/create-post", tags=["News"])
async def create_news(request: Request,
                      title: str = Form(...),
                      description: str = Form(...),
                      foto: UploadFile = File(...),
                      completed_date: Optional[str] = Form(None),
                      completed: Optional[bool] = Form(None),
                      genre: str = Form('other')
                      ) -> RedirectResponse:
    url = server.url_path_for('home')
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" start creating post')
    file_name = foto.filename
    if not foto.filename:
        file_name = "news.png"
    logger.debug(f'Line 87 -> message -- \"{file_name}\"')
    try:
        with open(f'{UPLOADED_IMG_PATH}/{file_name}', "wb") as uploaded_file:
            logger.debug(f'Line 90 -> message -- Open context manager {UPLOADED_IMG_PATH}/{file_name}')
            try:
                file_content = await foto.read()
                uploaded_file.write(file_content)
                uploaded_file.close()
            except Exception as e:
                logger.error(f'Error in line 96 in main.py -> {e}')
            #if
            create_post(title, description, foto.filename, genre, logger=logger)
    except Exception as error:
        logger.error(f'unluck to create new post // ERROR: {error}')
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.post("/update-news/", tags=["News"])
async def update_news(id: int = Form(...),
                      title: str = Form(""),
                      description: str = Form(""),
                      foto: UploadFile = File(""),
                      completed_date: Optional[str] = Form(None),
                      completed: Optional[bool] = Form(None),
                      genre: str = Form('other')
                      ):
    url = server.url_path_for('home')
    try:
        with open(f'{UPLOADED_IMG_PATH}/{foto.filename}', "wb") as uploaded_file:
            file_content = await foto.read()
            uploaded_file.write(file_content)
            uploaded_file.close()
            update_post(id, title, description, foto.filename, genre, logger=logger)
    except Exception as e:
        logger.error(f'error on updating news // ERROR: {e}')
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get('/news/about/{id}', tags=["News"])
async def about(request: Request, id: int):
    logger.info(
        f'client=\"{request.client.host}:{request.client.port}\" url=\"/news/about/{id}\" status=\"HTTP_200_OK\"')
    post = get_post_by_id(id, logger=logger)
    comments = get_comments_for_post(id, logger=logger)
    print(comments)
    return templates.TemplateResponse("about.html", {"request": request, "post": post, "comments": comments})


@server.get("/delete-post/", tags=["News"])
async def delete_news(title: str):
    post = get_post(title, logger=logger)
    delete_post(post, logger=logger)
    return RedirectResponse("/")


@server.get("/news-search", tags=["News"])
def search_news(request: Request, text: str):
    s = search_post(text, logger=logger)
    if_news = len(s)
    return templates.TemplateResponse("index.html", {"request": request, "news": s, "if_news": if_news})


@server.get('/important_news', tags=["News"])
async def important_news(request: Request):
    f_important_news = get_post_by_genre('Важное', logger=logger)
    if_news = len(f_important_news)
    return templates.TemplateResponse("index.html", {"request": request, "news": f_important_news,  "if_news": if_news})


@server.get('/event_news', tags=["News"])
async def events_news(request: Request):
    events = get_post_by_genre('Мероприятие', logger=logger)
    if_news = len(events)
    return templates.TemplateResponse("index.html", {"request": request, "news": events, "if_news": if_news})


@server.get('/data_news', tags=["News"])
async def data_news_func(request: Request):
    data_news = get_post_by_genre('Дата', logger=logger)
    if_news = len(data_news)
    return templates.TemplateResponse("index.html", {"request": request, "news": data_news , "if_news": if_news})


@server.get('/other_news', tags=["News"])
async def other_news_func(request: Request):
    other_news = get_post_by_genre('Обычное', logger=logger)
    if_news = len(other_news)
    return templates.TemplateResponse("index.html", {"request": request, "news": other_news, "if_news": if_news})


@server.post('/news/about/{id}', tags=["News"])
async def create_comment(id: int, text=Form(...), token=Form(...)):
    url = server.url_path_for('about', id=id)
    user = get_current_user(token)
    create_comment_for_post(news_id=id, owner_id=user.id, text=text, logger=logger)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get('/news/about/{news_id}/remove/{comment_id}', tags=["News"])
async def remove_comment(news_id: int, comment_id: int):
    url = server.url_path_for('about', id=news_id)
    remove_comment_for_post(news_id, comment_id)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get('/getcomments')
def get_comments(news_id: int):
    post = get_comments_for_post(news_id, logger=logger)
    return post


"""
__________________________ Teacher block ________________________________
"""


@server.get("/prepods", tags=["Prepods"])
def get_employees(request: Request):
    prepods = get_prepods(logger=logger)
    return templates.TemplateResponse("prepods.html", {"request": request, "prepods": prepods})


@server.post("/create-prepod", tags=["Prepods"])
async def create_prepods(title: str = Form(...),
                         description: str = Form(...),
                         owner_id: int = Form(...),
                         foto: UploadFile = File(...)
                         ) -> RedirectResponse:
    url = server.url_path_for('admin')
    with open(f'{UPLOADED_IMG_PATH_PREPODS}/{foto.filename}', "wb") as uploaded_file:
        file_content = await foto.read()
        uploaded_file.write(file_content)
        uploaded_file.close()
        create_prepod(title, description, foto.filename, owner_id, logger)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.post("/delete-prepod", tags=["Prepods"])
def delete_prepods(request: Request, fio: str = Form(...)) -> RedirectResponse:
    url = server.url_path_for('admin')
    prepod = get_prepod(fio, logger)
    try:
        delete_prepod(prepod, logger)
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" message=\"{fio} is deleted\"')
    except Exception as e:
        logger.error(e)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


"""
__________________________ Account's block ________________________________
"""


@server.get('/admin', tags=["Main"])
def admin(request: Request):
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" url=\"/admin\" status=\"HTTP_200_OK\"')
    news = get_posts(logger=logger)
    peoples = get_prepods(logger=logger)
    posts = get_file_from_db()
    users = get_users()
    return templates.TemplateResponse("admin.html", {"request": request, 
                                                     "news": news, 
                                                     "peoples": peoples, 
                                                     "posts": posts,
                                                     "users": users
                                                    })


@server.post('/change-user-role', tags=["Auth"])
def change_user_role(request: Request, user_id: int = Form(...), user_role: str = Form(...)):
    url = server.url_path_for('admin')
    update_user_role(id=user_id, new_role=user_role)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get('/link-register', tags=["Auth"])
def link_register(request: Request):
    return templates.TemplateResponse("registration.html", {"request": request})


@server.post("/register-user", tags=["Auth"])
def registration_new_user(data: str = Body(...)) -> RedirectResponse:
    url = server.url_path_for('home')
    a = data.split('&')
    username = a[0][9:]
    password = a[1][9:]
    user = get_user(username)
    if user:
        raise HTTPException(status_code=400, detail="Username already registered")
    create_user(username, password, "user")
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get("/user", tags=["Auth"])
async def get_user_info(token: str = Header(...)):
    user = get_current_user(token)
    if not user.avatar:
        user.avatar = "/app/static/img/users/user_default_4.png"
    return user


@server.post("/token", tags=["Auth"], response_model=Token)
async def login_for_access_token(request: Request, form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return templates.TemplateResponse("tmp.html", {"request": request,
                                                   "name": user.username,
                                                   "role": user.role,
                                                   "avatar": user.avatar,
                                                   "gitId": user.git_id,
                                                   "access_token": access_token,
                                                   })


@server.get("/auth", tags=["Auth"])
async def url_for_gitlab():
    authorization_url = (authorizationUrl +
                         f"?response_type=code&client_id={client_id}&redirect_uri={redirect_uri}&scope={scope}")
    return RedirectResponse(f"{authorization_url}")


@server.get("/auth/callback", tags=["Auth"])
async def gitlab_auth_callback(request: Request, code: str):
    data = {
        "grant_type": "authorization_code",
        "client_id": client_id,
        "client_secret": client_secret,
        "code": code,
        "redirect_uri": redirect_uri,
    }
    async with httpx.AsyncClient() as client:
        response = await client.post(tokenUrl, data=data)
    if response.status_code != 200:
        raise HTTPException(status_code=400, detail="Failed to get access token")
    token = response.json()["access_token"]
    headers = {"Authorization": f"Bearer {token}"}
    async with httpx.AsyncClient() as client:
        response = await client.get("https://gitlab.com/api/v4/user", headers=headers)
    if response.status_code != 200:
        raise HTTPException(status_code=400, detail="Failed to get user profile")
    user_profile = response.json()
    user = auth_user_git(user_profile)
    try:
        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(
            data={"sub": user.username}, expires_delta=access_token_expires
        )
        return templates.TemplateResponse("tmp.html", {"request": request, "access_token": access_token})
    except Exception as e:
        logger.error(f"something wrong with git. err {e}")
        RedirectResponse("/")


@server.get("/link/{token}/{git_id}/{git_username}/{git_name}", tags=["Auth"])
def link(token: str, git_id: str, git_username: str, git_name: str):
    git_info = {'git_id': git_id, "git_username": git_username, "git_name": git_name}
    user = get_current_user(token)
    git_update_user(user.id, git_info)
    return RedirectResponse("/")


@server.post('/change-avatar', tags=["Auth"])
async def change_avatar(request: Request, upload_image: UploadFile = File(...), token: str = Form(...)):
    url = server.url_path_for('home')
    current_user = get_current_user(token)
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" message=\"Starting upload new avatar')
    data = await upload_image.read()
    try:
        os.mkdir(UPLOADED_IMG_PATH_USERS)
    except FileExistsError:
        pass
    save_to = UPLOADED_IMG_PATH_USERS / upload_image.filename
    try:
        with open(save_to, 'ab') as f:
            f.write(data)
    except RuntimeError:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" message=\"Can\'t save avatar\"')
        return
    logger.info(
        f'client=\"{request.client.host}:{request.client.port}'
        f'\" message=\"avatar successful uploaded\" status=\"HTTP_200_OK\"')
    change_avatar_url(current_user, upload_image.filename)
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get('/avatar')
async def get_avatar(id: int = Header(...)):
    user_avatar = get_user_by_id(id)
    return {'url': user_avatar.avatar, 'username': user_avatar.username}


"""
__________________________ Library block ________________________________
"""


@server.get('/lib', tags=["Library"])
def library(request: Request):
    docs = get_file_from_db()[::-1]
    return templates.TemplateResponse(name="lib.html", context={"request": request, "docs": docs})


@server.post("/lib/upload", tags=["Library"], status_code=status.HTTP_200_OK)
async def upload_file_func(response: Response,
                           annotation: str = Form("описания нет"),
                           tag: str = Form("Статья"),
                           upload_file: UploadFile = File(...),
                           ):
    url = server.url_path_for('library')
    file = upload_file
    # full_name = format_filename(file, annotation, file.filename)
    full_name = file.filename
    await save_file_to_uploads(file, tag)
    file_size = human_read_format(get_file_size(file.filename, tag))
    file_info_from_db = get_file_from_db(file.filename)
    if not file_info_from_db:
        response.status_code = status.HTTP_201_CREATED
        add_file_to_db(
            annotation=annotation,
            full_name=full_name,
            tag=tag,
            file_size=file_size,
            file=file,
        )
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get("/lib/show/{name}", tags=["Library"])
def watch_online(name):
    url = server.url_path_for('library')
    file = get_file_from_db(name)
    if file.mime_type == "application/pdf":
        pdf = get_content_file(file.name, file.tag)
        response = Response(pdf, media_type="application/pdf")
        return response
    else:
        return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get("/lib/download/{name}", tags=["Library"], status_code=status.HTTP_200_OK)
async def download_file(
        response: Response,
        name: str,
):
    file_info_from_db = get_file_from_db(name)

    if file_info_from_db:
        file_resp = FileResponse(UPLOADED_FILES_PATH / file_info_from_db.tag / file_info_from_db.name,
                                 media_type=file_info_from_db.mime_type,
                                 filename=file_info_from_db.name)
        response.status_code = status.HTTP_200_OK
        return file_resp
    else:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {'msg': 'File not found'}


# ____________________ unused ___________________
@server.post("/lib/delete", tags=["Library"])
async def delete_file(response: Response, name: str = Form(...)):
    url = server.url_path_for('library')
    file_info_from_db = get_file_from_db(name)
    if file_info_from_db:
        delete_file_from_db(file_info_from_db)
        delete_file_from_uploads(file_info_from_db.name, file_info_from_db.tag)
        response.status_code = status.HTTP_200_OK
        return RedirectResponse(url=url, status_code=HTTP_302_FOUND)
    else:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {'msg': f'File does not exist'}


@server.get("/lib/search/subtr-{substr}", tags=["Library"])
def search_substr(substr: str):
    names = []
    files = get_file_from_db()
    for file in files:
        f = None
        if file.mime_type == "application/pdf":
            f = search_text_in_pdf(file, substr)
        elif file.mime_type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            f = search_text_in_docx(file, substr)
        if f is not None:
            names.append(file.name)
    return names


@server.get("/lib/search/tag-{substr}", tags=["Library"])
def search_by_tags(substr: str):
    names = []
    files = get_file_from_db()
    for file in files:
        if file.tag == substr:
            names.append(file.name)
    return names


@server.get("/lib/search/name-{substr}", tags=["Library"])
def search_by_name(substr: str):
    names = []
    files = get_file_from_db()
    for file in files:
        name = str(Path(file.name).with_suffix(''))
        if substr in name:
            names.append(file.name)
    return names
