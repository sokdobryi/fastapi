import os
import app.db_models

from datetime import datetime
from app.db_connect import SessionLocal
from app.db_models import News, Comment


UPLOADED_FILES_PATH = "static/img/news"


def create_post(title: str, description: str, filename: str, genre: str, logger):
    """
    Function for create post
    :param title: title of new news
    :param description: description of new news
    :param filename: filename in fyle system
    :param genre: genre of new news
    :param logger: logger
    :return: None
    """
    try:
        db = SessionLocal()
        create_news = News(title=title, description=description, url_photo=filename, genre=genre)
        db.add(create_news)
        db.commit()
        db.refresh(create_news)
        db.close()
        logger.info(f'Post successful create with params {title}, {description}, {genre}')
    except Exception as e:
        logger.error(f"Error in line 31 in news.py // {e}")


def get_posts(logger):
    """
    Function for get posts from database
    :param logger: logger
    :return: dictionary of posts
    """
    try:
        db = SessionLocal()
        news = db.query(News).all()
        db.close()
        return news
    except Exception as e:
        logger.error(f'Error in line 45 in news.py // {e}')


def get_post(title: str, logger):
    """
    Function for getting post with this title
    :param title: post-title
    :param logger: logger
    :return: model of post
    """
    try:
        db = SessionLocal()
        post = db.query(app.db_models.News).filter(app.db_models.News.title == title).first()
        db.close()
        return post
    except Exception as e:
        logger.error(f'Error in line 61 in news.py // {e}')


def get_post_by_id(id: int, logger):
    """
    Function for getting post by id
    :param id: news-id
    :param logger: logger
    :return: model of post
    """
    try:
        db = SessionLocal()
        post = db.query(app.db_models.News).filter(app.db_models.News.id == id).first()
        db.close()
        return post
    except Exception as e:
        logger.error(f'Error in line 76 in news.py // {e}')


def update_post(id: int, title: str, description: str, url_photo: str, genre: str, logger):
    """
    Function for updating post
    If one or more parameters are null, they are not changed
    :param id: post-id
    :param title: post-title
    :param description: post-description
    :param url_photo: post-url_photo
    :param genre: post-genre
    :param logger: logger
    :return: model of new post
    """
    try:
        db = SessionLocal()
        update_file = db.query(app.db_models.News).filter(app.db_models.News.id == id).first()
        if title != "":
            update_file.title = title
        if description != "":
            update_file.description = description
        if url_photo != "":
            update_file.url_photo = url_photo
        if genre != "":
            update_file.genre = genre
        update_file.created_date = datetime.now().date()
        db.commit()
        db.refresh(update_file)
        logger.info(f'Post {id} successful update')
        return update_file
    except Exception as e:
        logger.error(f'Error in line 106 in news.py // {e}')


def delete_post(post, logger):
    """
    Function for deleting post from database
    :param post: model of post for deleting
    :param logger: logger
    :return: None
    """
    try:
        db = SessionLocal()
        os.remove(f'{UPLOADED_FILES_PATH}/{post.url_photo}')
        db.delete(post)
        db.commit()
        db.close()
        logger.info(f'Post {post.title} successful delete')
    except Exception as e:
        logger.error(f'Error in line 125 in news.py // {e}')


def search_post(substr: str, logger):
    """
    Function for searching posts in database
    :param substr: The text that is being searched for
    :param logger: logger
    :return: set of news
    """
    try:
        db = SessionLocal()
        search = db.query(app.db_models.News).filter(app.db_models.News.title.contains(substr)).all()
        search += db.query(app.db_models.News).filter(app.db_models.News.description.contains(substr)).all()
        return set(search)
    except Exception as e:
        logger.error(f'Error in line 141 in news.py // {e}')


async def save_file_to_uploads(file, logger):
    """
    Function for saving pictures of news
    :param file: image of post
    :param logger: logger
    :return: path of image
    """
    try:
        with open(f'{UPLOADED_FILES_PATH}{file.filename}', "wb") as uploaded_file:
            file_content = await file.read()
            uploaded_file.write(file_content)
            uploaded_file.close()
        return f'{UPLOADED_FILES_PATH}{file.filename}'
    except Exception as e:
        logger.error(f'Error in line 158 in news.py // {e}')


def get_post_by_genre(genre: str, logger):
    """
    Function for getting posts by genre
    :param genre: genre-of searching news
    :param logger: logger
    :return: list of posts
    """
    try:
        db = SessionLocal()
        posts = db.query(app.db_models.News).filter(app.db_models.News.genre == genre).all()
        db.close()
        return posts
    except Exception as e:
        logger.error(f'Error in line 173 in news.py // {e}')


def create_comment_for_post(news_id: int, owner_id: int, text: str, logger):
    """
    Function for create comment for post
    :param news_id: id of post
    :param owner_id: id of the commentator
    :param text: comment
    :param logger: logger
    :return: model of new comment
    """
    try:
        db = SessionLocal()
        created_comment = Comment(text=text, news_id=news_id, owner_id=owner_id)
        db.add(created_comment)
        db.commit()
        db.refresh(created_comment)
        db.close()
        logger.info(f'Comment created by {owner_id} for news {news_id}')
        return created_comment
    except Exception as e:
        logger.error(f'Error in line 194 in news.py // {e}')


def remove_comment_for_post(news_id: int, comment_id: int):
    """
    Function for remove comment for post by id
    :param news_id: id of post
    :param comment_id: id of comment
    :return: None
    """
    db = SessionLocal()
    comments = db.query(app.db_models.Comment).filter(app.db_models.Comment.news_id == news_id).all()
    for i in comments:
        if i.id == comment_id:
            db.delete(i)
            db.commit()
            db.close()
            return
    pass


def get_comments_for_post(news_id: int, logger):
    """
    Function for getting comments
    :param news_id: id of news
    :param logger: logger
    :return: list of comments
    """
    try:
        db = SessionLocal()
        comments = db.query(app.db_models.Comment).filter(app.db_models.Comment.news_id == news_id).all()
        return comments
    except Exception as e:
        logger.error(f'Error in line 209 in news.py // {e}')
