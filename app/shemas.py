from typing import Optional
from app.db_models import Roles
from pydantic import BaseModel


class RegisterUser(BaseModel):
    username: str
    password: str


class RegisterPrepod(BaseModel):
    username: str
    password: str
    data: Optional[str] = None
    avatar: Optional[str] = None


class ChangeUser(BaseModel):
    id: str
    username: str
    role: Roles

    class Config:
        use_enum_valuse = True